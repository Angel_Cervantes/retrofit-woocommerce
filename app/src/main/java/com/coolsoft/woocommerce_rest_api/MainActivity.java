package com.coolsoft.woocommerce_rest_api;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;


import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    TextView resultadoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        resultadoTextView = (TextView)findViewById(R.id.resultadoTextView);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder(); /* Para crear un OkHttpClient con la configuración predeterminada,
                                                                                  * utilice el constructor predeterminado. O crear una instancia
                                                                                  *configurada con OkHttpClient. Builder.
                                                                                  */
        final Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(ServiceGenerator.API_BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create()).build();/*Para enviar solicitudes de red a una API, tenemos que utilizar
                                                                                    *Retrofit builder y especificar la URL base para el servicio.
                                                                                    */

        ServiceGenerator servicio = retrofit.create(ServiceGenerator.class);
        Call<List<Categoria>> resouesta = servicio.ListaCategoria();

        resouesta.enqueue(new Callback<List<Categoria>>() {
            @Override
            public void onResponse(Call<List<Categoria>> call, Response<List<Categoria>> response) {
                if(!response.isSuccessful())
                {
                    resultadoTextView.setText("Error"+response.code());
                }
                else
                {
                    List<Categoria> c = response.body();
                    resultadoTextView.setText(c.toString());
                }

            }

            @Override
            public void onFailure(Call<List<Categoria>> call, Throwable t) {
                resultadoTextView.setText("Error");
            }
        });






    }
}
