package com.coolsoft.woocommerce_rest_api;

import retrofit2.Call;
import retrofit2.http.GET;

import java.util.Date;

import java.security.SecureRandom;
import java.math.BigInteger;
import java.util.List;

/**
 * Created by Angel on 12/06/2016.
 */
public interface ServiceGenerator {

    Date date= new Date();
    SecureRandom random = new SecureRandom();
    public static final String API_BASE_URL = "http://practicas.idwasoft.com/wp-json/wc/v1/products/categories?parent=12&amp;oauth_consumer_key=ck_a8f1efe78599cbd63da03018ff1117354a656568&amp;oauth_signature_method=HMAC-SHA1&amp;oauth_timestamp="
            +date.getTime()+"&amp;oauth_nonce="
            +new BigInteger(130, random).toString(32)+"&amp;oauth_version=1.0&amp;";//URL base siempre deben terminar en /.

    @GET("oauth_signature=y0eIfdBHcskt3HGtD5zsrI0jiVM=")
    Call<List<Categoria>> ListaCategoria();



}